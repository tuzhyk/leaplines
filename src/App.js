import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';
import ReviewPage from './Review/ReviewPage';
import MyTeam from './MyTeam/MyTeam';
import Dashboard from './Dashboard/DashboardPage';
// import ElementsPage from '@/components/pages/ElementsPage';
// import NotFoundPage from '@/components/pages/NotFoundPage';
// import {
//   HOME_PAGE,
// } from '@/constants';

export default class SimpleAppBar extends React.Component {
  render() {
    // console.log('<<', this.props);
    return (
      <Router>

        <div>
          <AnimatedSwitch
            atEnter={{ opacity: 0.95 }}
            atLeave={{ opacity: 0.95 }}
            atActive={{ opacity: 1 }}
          >
            {/* <Switch> */}
            <Route path="/review" exact component={ReviewPage} />
            <Route path="/team" exact component={MyTeam} />
            <Route path="/dashboard" exact component={Dashboard} />
            {/* <Route path={'/elements'} component={ElementsPage} /> */}
            {/* </Switch> */}
          </AnimatedSwitch>
        </div>
      </Router>
    );
  }
}
