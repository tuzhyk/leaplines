import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import purple from '@material-ui/core/colors/purple';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import SortUp from '@material-ui/icons/ArrowDropDown';
import SortDown from '@material-ui/icons/ArrowDropUp';

import ListItem from '../ListItem/index';

const color = createMuiTheme({
  palette: {
    primary: purple,
    secondary: {
      main: '#df2374',
    },
    inherit: {
      main: '#212121',
    },
  },
  appBar: {
    bottom: 'auto',
    top: 0,
  },
});

const styles = () => ({

  listButton: {
    paddingRight: 16,
    width: 84,
  },
  listItem: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 3,
    marginBottom: 3,
    alignItems: 'flex-start',
  },
  root: {
    flexGrow: 1,
  },
  palette: color.palette,
  userName: {
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.25,
    letterSpacing: 'normal',
    color: '#232221',
  },
  userLessons: {
    height: 20,
    fontFamily: 'Palanquin',
    opacity: 0.5,
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 'normal',
    color: '#212121',
  },
  list: {
    paddingTop: 0,
  },
  divider: {
    marginLeft: 20,
    marginRight: 20,
    height: 2,
  },
  unassigned: {
    minHeight: 70,
    alignItems: 'center',
    display: 'inline-flex',
    width: '100%',
    backgroundColor: '#df2374',
  },
  unassignedText: {
    display: 'inline-flex',
    paddingLeft: 20,
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.43,
    letterSpacing: 'normal',
    color: '#ffffff',
  },
  unassignedArrow: {
    position: 'absolute',
    right: 14,
    color: '#ffffff',
  },
  employeesNum: {
    position: 'relative',
    paddingLeft: 20,
    top: 15,
    width: 57,
    height: 40,
    fontFamily: 'Palanquin',
    fontSize: 30,
    fontWeight: 600,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.33,
    letterSpacing: 'normal',
    color: '#232221',
  },
  employees: {
    minHeight: 100,
  },
  employeesText: {
    position: 'relative',
    paddingLeft: 20,
    top: 15,
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.43,
    letterSpacing: 'normal',
    color: '#232221',
  },
  reviewListHeader: {
    minHeight: 60,
    backgroundColor: '#f2f2f3',
    display: 'inline-flex',
    alignItems: 'center',
    // width:375,
    width: '100%',
  },
  reviewListTitle: {
    height: 20,
    paddingLeft: 20,
    fontFamily: 'Palanquin',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.25,
    letterSpacing: 'normal',
    color: '#232221',
  },
  listSort: {
    position: 'absolute',
    right: 16,
    textAlign: 'right',
    color: '#212121',
    height: 20,
    opacity: 0.5,
    fontFamily: 'Palanquin',
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 'normal',
    textTransform: 'none',
  },
  listBtn: {
    position: 'absolute',
    right: 19,
    minWidth: 0,
    padding: 0,
    height: 20,
  },
  sortIcon: {
    // position: 'absolute',
    // right: 19,
      position: 'relative',
      left: 24,
      bottom: 20,
  
  },
  //   reviewListSortIcon:{
  //     minHeight:60,
  //     backgroundColor:'#f2f2f3',
  //     display:'inline-flex',
  //     height: 20,
  //       fontFamily: 'Palanquin',
  //       fontSize: 16,
  //       fontWeight: 'bold',
  //       fontStyle: 'normal',
  //       fontStretch: 'normal',
  //       lineHeight: 1.25,
  //       letterSpacing: 'normal',
  //       color: '#232221',
  //       },
  knowledgeListHeader: {
    minHeight: 60,

    width: '100%',
  },
  knowledgeListTitle: {
    height: 20,
    paddingLeft: 20,
    paddingTop: 20,
    fontFamily: 'Palanquin',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.25,
    letterSpacing: 'normal',
    color: '#232221',
  },
});


class ListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sort: 'true',
    };
  }

  changeSort=() => {
    const { sort } = this.state;
    if (sort) {
      this.setState({
        sort: '',
      });
    } else {
      this.setState({
        sort: 'true',
      });
    }
  }

  compare=(a, b) => {
    const { sort } = this.state;
    if (a.percent > b.percent && sort) {
      return 1;
    }
    if (a.percent < b.percent && sort) {
      return -1;
    }
    if (a.percent > b.percent && !sort) {
      return -1;
    }
    if (a.percent < b.percent && !sort) {
      return 1;
    }
    return 0;
  }

  render() {
    const {
      classes, type, list, title,
    } = this.props;
    if (type === 'sort') {
      list.sort(this.compare);
      return (
        <div className={classes.root}>
          <div className={classes.reviewListHeader}>
            <Typography className={classes.reviewListTitle} variant="h4" color="inherit">
              {title}
            </Typography>
            <Button disableRipple disableTouchRipple variant=" " className={classes.listBtn} onClick={this.changeSort} style={{ backgroundColor: 'transparent' }}>
              <Typography className={classes.listSort} variant="h4" color="inherit">
      Progress
                { this.state.sort ? <SortDown
                  className={classes.sortIcon}
                /> :  <SortUp
                  className={classes.sortIcon}
                />}
              </Typography>

            </Button>
          </div>
          <List component="nav" className={classes.list}>
            {list.map((item, index) => (
              <React.Fragment key={item.name}>
                <ListItem type="user" item={item} />
                {index !== list.length - 1 && (
                  <Divider className={classes.divider} />
                )}
              </React.Fragment>
            ))}

          </List>
        </div>
      );
    }
    if (type === 'review') {
      return (
        <div className={classes.root}>
          <List component="nav" className={classes.list}>
            {list.map(item => (
              <React.Fragment key={item.name}>
                <ListItem type="review" item={item} />
                <Divider className={classes.divider} />
              </React.Fragment>
            ))}
          </List>
        </div>
      );
    }
    if (type === 'knowledge') {
      return (
        <div className={classes.root}>
          <div className={classes.knowledgeListHeader}>
            <Typography className={classes.knowledgeListTitle} variant="h4" color="inherit">
              {title}
            </Typography>
          </div>
          <List component="nav" className={classes.list}>
            {list.map((item, index) => (
              <React.Fragment key={item.name}>
                <ListItem type="knowledge" item={item} />
                {index !== list.length - 1 && (
                <Divider className={classes.divider} />
                )}
              </React.Fragment>
            ))}
          </List>
        </div>
      );
    }
    return 0;
  }
}

export default withStyles(styles)(ListComponent);
