import React from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../Header/Header';
import MainCard from '../MainCard/index';

import List from '../List/index';
import { knowledge } from '../Mocks/knowledge';

const styles = () => ({
  root: {
    flexGrow: 1,
  },
});

const Dashboard = (props) => {
  const { classes } = props;
  const page = {
    dash: 'dash',
  };
  return (
    <div className={classes.root}>
      <Header page={page} />
      <MainCard />
      <List
        type="knowledge"
        title="Knowledge of your team"
        list={knowledge}
      />
    </div>
  );
};

export default withStyles(styles)(Dashboard);
