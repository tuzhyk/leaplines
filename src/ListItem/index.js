import React from 'react';
// import PropTypes from 'prop-types';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import purple from '@material-ui/core/colors/purple';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import classnames from 'classnames';
import ProgressBar from '../ProgressBar/index';

import AlertIcon from '../Icons/allert.svg';
import UpToDateIcon from '../Icons/up-to-date.svg';

const color = createMuiTheme({
  palette: {
    primary: purple,
    secondary: {
      main: '#df2374',
    },
    inherit: {
      main: '#212121',
    },
  },
  appBar: {
    bottom: 'auto',
    top: 0,
  },
});

const UserInfo = styled.div`
padding-left: 15px;
width: 100%;
`;

const styles = () => ({
  userListItem: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 17,
    paddingBottom: 17,
    marginTop: 3,
    marginBottom: 3,
    alignItems: 'flex-start',
  },
  reviewListItem: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 17,
    paddingBottom: 17,
    marginTop: 3,
    marginBottom: 3,
  },
  knowledgeListItem: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 17,
    paddingBottom: 17,
    marginTop: 3,
    marginBottom: 3,
    display: 'block',
    alignItems: 'flex-start',
  },
  root: {
    flexGrow: 1,
  },
  palette: color.palette,
  userName: {
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.25,
    letterSpacing: 'normal',
    color: '#232221',
  },
  userData: {
    height: 20,
    fontFamily: 'Palanquin',
    opacity: 0.5,
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 'normal',
    color: '#212121',
  },
  arrowRight: {
    position: 'absolute',
    right: 14,
    top: 16,
    width: 20,
    height: 20,
  },
  listButton: {
    paddingRight: 16,
    width: 84,
  },
  userReviewBtn: {
    borderRadius: 0,
    boxShadow: '0px 0px 0px 0px',
    backgroundColor: '#383838',
    width: 84,
    height: 40,
    fontFamily: 'Palanquin',
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 'normal',
    textAlign: 'center',
    color: '#ffffff',
    textTransform: 'none',
  },
  knowledgeItemTitle: {
    display: 'flex',
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 16,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.25,
    letterSpacing: 'normal',
    textAlign: 'center',
    position: 'absolute',
    top: 10,
    left: 44,

    // alignItems: 'center',
  },
  itemRoundIcon: {
    width: 12,
    height: 12,
    borderRadius: '50%',
    marginRight: 12,
  },
  informationContent: {
    display: 'flex',
    alignItems: 'center',
    margin: '12px 0px 0px 0px',
  },
  informationContentIcon: {
    width: 20,
    height: 20,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    marginRight: 10,
  },
  informationContentDescription: {
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 13,
    fontWeight: 600,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 'normal',
    color: '#85c068',
  },
  warning: {
    color: '#e23636',
  },
});


const ListItemComponent = (props) => {
  const { classes, type, item } = props;
  if (type === 'user') {
    return (
      <ListItem disableRipple disableTouchRipple key="1" button className={classes.userListItem} style={{ backgroundColor: 'transparent' }}>
        <Avatar
          alt={item.name}
          src="https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
        />
        <UserInfo>
          <ListItemText classes={{ primary: classes.userName, secondary: classes.userData }} primary={item.name} secondary={`Last login: ${item.lastLogin}`} />
          <div>
            <ProgressBar
              value={item.percent}
            />
          </div>
        </UserInfo>
        <div>
          <ArrowRight className={classes.arrowRight} />
        </div>

      </ListItem>
    );
  }
  if (type === 'review') {
    return (
      <ListItem disableRipple disableTouchRipple key="1" button className={classes.reviewListItem} style={{ backgroundColor: 'transparent' }}>
        <Avatar
          alt="Remy Sharp"
          src="https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
        />
        <ListItemText classes={{ primary: classes.userName, secondary: classes.userData }} primary={item.name} secondary={`${item.lessons} lessons`} />
        <ListItemSecondaryAction className={classes.listButton}>
          <Button disableRipple disableTouchRipple variant="contained" color="inherit" className={classes.userReviewBtn} style={{ backgroundColor: '#383838' }}>Review</Button>
        </ListItemSecondaryAction>

      </ListItem>
    );
  }
  if (type === 'knowledge') {
    return (
      <ListItem disableRipple disableTouchRipple key="1" button className={classes.knowledgeListItem} style={{ backgroundColor: 'transparent' }}>
        <div>
          <div
            className={classes.itemRoundIcon}
            style={{ backgroundColor: item.color }}
          />
          <Typography className={classes.knowledgeItemTitle} variant="h4" color="inherit">
            {item.name}
          </Typography>

        </div>
        <ArrowRight className={classes.arrowRight} />
        <ProgressBar
          color={item.color}
          value={item.percent}
          type="height"
        />
        <div className={classes.informationContent}>
          <div
            className={classes.informationContentIcon}
            style={{ backgroundImage: `url('${item.count > 0 ? AlertIcon : UpToDateIcon}')` }}
          />
          <div
            className={
          classnames(classes.informationContentDescription, item.count > 0 && classes.warning)
        }
          >
            {item.count > 0 ? `${Math.abs(item.count)} employees are behind` : 'Completely up-to-date'}
          </div>
        </div>
        <div />

      </ListItem>
    );
  }
  return 0;
};

export default withStyles(styles)(ListItemComponent);
