import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import { loadCSS } from 'fg-loadcss/src/loadCSS';
import classNames from 'classnames';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';

const color = createMuiTheme({
  palette: {
    primary: {
      light: '#2e2e2e',
      dark: '#2e2e2e',
      contrastText: '#2e2e2e',
      main: '#2e2e2e',
    },
    secondary: {
      main: '#df2374',
    },
    inherit: {
      main: '#212121',
    },
  },
  appBar: {
    bottom: 'auto',
    top: 0,
  },
});

const styles = () => ({
  badge: {
    top: 10,
    right: -7,
    width: 20,
    borderRadius: 8,
    height: 15,
    backgroundColor: '#df2374',
    fontFamily: 'Palanquin',
    fontSize: 10,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    alignContent: 'flex-end',
    letterSpacing: 'normal',
    textAlign: 'center',
    color: '#ffffff',
  },
  toolbar: {
    minHeight: 48,
    paddingLeft: 12,
  },
  headerNavigation: {
    top: 65,
    height: 48,
  },
  headerNavigationBtn: {
    paddingRight: 8,
    marginRight: 8,
    paddingLeft: 8,
    padding: 0,
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.54,
    letterSpacing: 0.5,
    color: '#999999',
  },
  linkDecoration: {
    textDecoration: 'none',
  },
  header: {
    height: 65,
    backgroundColor: '#df2374',
  },
  headerContainer: {
    height: 113,
    backgroundColor: 'transparent',
  },
  icon: {
    top: 5,
    paddingLeft: 4,
    width: '15px',
    height: '14px',
    position: 'relative',
  },
  root: {
    flexGrow: 1,
  },
  headerText: {
    paddingLeft: 9,
    width: 252,
    height: 24,
    fontFamily: 'Palanquin',
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 2.46,
    letterSpacing: 1,
    color: '#ffffff',
  },
  palette: color.palette,
  headerNavigationBtnSelected: {
    color: '#2e2e2e',
  },
});


class Header extends Component {
  componentDidMount() {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
      document.querySelector('#insertion-point-jss'),
    );
  }

  render() {
    const { classes, page } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.headerContainer}>
          <AppBar position="fixed" top="50px" color="secondary" className={classes.header}>
            <Toolbar>
              <Icon className={classNames(classes.icon, 'fas fa-graduation-cap')} style={{ fontSize: 13 }} />
              <Typography variant="h8" color="inherit" className={classes.headerText}>

              LEAPLINES ACADEMY MANAGER
              </Typography>
            </Toolbar>
          </AppBar>
          {/* <div className={classes.root} /> */}
          <AppBar position="fixed" color="default" className={classes.headerNavigation}>
            <Toolbar className={classes.toolbar}>
              <div>
                <Link to="/dashboard" className={classes.linkDecoration}>
                  <Button disableRipple disableTouchRipple variant=" " color="primary" className={classes.headerNavigationBtn} style={{ backgroundColor: 'transparent' }}>
                    <Typography variant="h8" className={page.dash ? classes.headerNavigationBtnSelected : ''}>
                  DASHBOARD
                    </Typography>
                  </Button>
                </Link>
                <Link to="/team" className={classes.linkDecoration}>
                  <Button disableRipple disableTouchRipple variant=" " color="primary" className={classes.headerNavigationBtn} style={{ backgroundColor: 'transparent' }}>
                    <Typography variant="h8" style={{ textDecoration: 'none' }} className={page.team ? classes.headerNavigationBtnSelected : ''}>
                  MY TEAM
                    </Typography>
                  </Button>
                </Link>
                <Link to="/review" className={classes.linkDecoration}>
                  <Badge
                    color="secondary"
                    badgeContent={3}
                    classes={{ badge: classes.badge }}
                  >


                    <Button disableRipple disableTouchRipple variant=" " color="primary" className={classes.headerNavigationBtn} style={{ backgroundColor: 'transparent' }}>
                      <Typography variant="h8" className={page.review ? classes.headerNavigationBtnSelected : ''}>
                    REVIEW
                      </Typography>
                    </Button>
                  </Badge>
                </Link>
              </div>
            </Toolbar>
          </AppBar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(Header);
