import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  padding-top: 2px;
  justify-content: space-between;
`;
const Progress = styled.div`
  margin-top: 10px;
  height: 4px;
  width: 86%;
  border-radius: 20px;
  background-color: #f2f2f3;
  min-width: 238px;
`;
const ProgressFilled = styled.div`
  height: 100%;
  border-radius: 20px;
`;

const Percentage = styled.div`
  text-align: right;
  width: 33px;
  height: 20px;
  font-family: Palanquin;
  font-size: 14px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.43;
  letter-spacing: normal;
  text-align: right;
  color: #212121;
`;
const ProgressBar = (props) => {
  const { color, value, type } = props;
  let height;
  let percentStyle;
  let marginTop;
  if (type === 'height') {
    marginTop = '15px';
    height = '8px';
    percentStyle = {
      position: 'absolute',
      top: '37px',
      right: '20px',
    };
  } else {
    height = '4px';
  }
  const formattedValue = `${value}%`;
  return (
    <Wrapper>
      <Progress
        style={{
          height,
          marginTop,
        }}
      >
        <ProgressFilled
          style={{
            backgroundColor: color,
            width: `${value}%`,
          }}
        />
      </Progress>
      <Percentage className="percentage" style={percentStyle}>
        {formattedValue}
      </Percentage>
    </Wrapper>
  );
};

ProgressBar.propTypes = {
  color: PropTypes.string,
  value: PropTypes.number,
};
ProgressBar.defaultProps = {
  color: '#df2374',
  value: 40,
};

export default ProgressBar;
