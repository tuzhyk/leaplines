import React from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Header from '../Header/Header';
import MainCard from '../MainCard/index';

import List from '../List/index';
import { employees } from '../Mocks/employees';
import Users from '../Icons/team.svg';

const ColoredFont = styled.span`
color: #df2374;
`;
const styles = () => ({
  root: {
    flexGrow: 1,
  },

  unassigned: {
    minHeight: 70,
    alignItems: 'center',
    display: 'inline-flex',
    width: '100%',
    backgroundColor: '#df2374',
  },
  unassignedText: {
    display: 'inline-flex',
    paddingLeft: 11,
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    letterSpacing: 'normal',
    color: '#ffffff',
  },
  unassignedArrow: {
    position: 'absolute',
    right: 14,
    color: '#ffffff',
    width: 20,
    height: 20,
  },
  employeesNum: {
    position: 'relative',
    paddingLeft: 20,
    top: 15,
    width: 57,
    height: 40,
    fontFamily: 'Palanquin',
    fontSize: 30,
    fontWeight: 600,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.33,
    letterSpacing: 'normal',
    color: '#232221',
  },
  employees: {
    minHeight: 100,
  },
  employeesText: {
    position: 'relative',
    paddingLeft: 20,
    top: 20,
    height: 20,
    fontFamily: 'Palanquin',
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.43,
    letterSpacing: 'normal',
    color: '#232221',
  },
  informationContentIcon: {
    width: 19,
    height: 17,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    marginLeft: 20,
  },
});

const MyTeamPage = (props) => {
  const { classes } = props;
  const page = {
    team: 'team',
  };
  return (
    <div className={classes.root}>
      <Header page={page} />
      <MainCard />
      <div role="button" style={{ backgroundColor: 'transparent' }}>
        <Paper elevation={1} square="true" className={classes.unassigned}>
          <span
            className={classes.informationContentIcon}
            style={{ backgroundImage: `url('${Users}')` }}
          />
          <Typography variant="h5" component="h3" className={classes.unassignedText}>
      4 employees are unassigned to a team
          </Typography>
          <ArrowRight className={classes.unassignedArrow} />
        </Paper>
      </div>
      <Paper elevation={1} square="true" className={classes.employees}>

        <Typography variant="h5" component="p" className={classes.employeesNum}>
          <ColoredFont>3</ColoredFont>
/12
        </Typography>
        <Typography component="p" className={classes.employeesText}>
        4 Employees are completely up-to-date
        </Typography>
      </Paper>
      <List type="sort" title="Employees" list={employees} />
    </div>
  );
};

export default withStyles(styles)(MyTeamPage);
