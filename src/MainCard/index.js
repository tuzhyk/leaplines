import React from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';

const styles = () => ({
  media: {
    minHeight: 160,
    borderRadius: 0,
  },
  pict: {
    paddingLeft: 20,
    paddingTop: 72,
    height: 24,
    fontFamily: 'Palanquin',
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.6,
    letterSpacing: 'normal',
    color: '#ffffff',
  },
  pictt: {
    paddingLeft: 20,
    paddingTop: 4,
    height: 40,
    fontFamily: 'Palanquin',
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.43,
    letterSpacing: 'normal',
    color: '#ffffff',
  },
});


const MainCard = (props) => {
  const { classes } = props;
  return (
    <CardMedia
      component="div"
      className={classes.media}
      image="https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
      title="Contemplative Reptile"
    >
      <Typography className={classes.pict} variant="h4" color="inherit">
        Give feedback!
      </Typography>
      <Typography className={classes.pictt} variant="h10" color="inherit">
        Coach your employees and review the results of their lessons to improve their perfomance.
      </Typography>
    </CardMedia>
  );
};

export default withStyles(styles)(MainCard);
