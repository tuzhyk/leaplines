import React from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../Header/Header';
import MainCard from '../MainCard/index';

import List from '../List/index';
import { reviewEmployees } from '../Mocks/reviewEmployees';


const styles = () => ({
  root: {
    flexGrow: 1,
  },
});


const ReviewPage = (props) => {
  const { classes } = props;
  const page = {
    review: 'review',
  };
  return (
    <div className={classes.root}>
      <Header page={page} />
      <MainCard />
      <List type="review" list={reviewEmployees} />
    </div>
  );
};

export default withStyles(styles)(ReviewPage);
