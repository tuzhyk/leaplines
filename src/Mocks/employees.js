/* eslint-disable import/prefer-default-export */
export const employees = [
  {
    name: 'Thea Oldenburger',
    lastLogin: 'November 8th, 2018',
    percent: 80,
  },
  {
    name: 'Vera Bosch',
    lastLogin: 'November 8th, 2018',
    percent: 60,
  },
  {
    name: 'Sharine Dulos',
    lastLogin: 'November 8th, 2018',
    percent: 67,
  },
  {
    name: 'Roya de Wit',
    lastLogin: 'November 8th, 2018',
    percent: 100,
  },
];
