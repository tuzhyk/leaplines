/* eslint-disable import/prefer-default-export */
export const reviewEmployees = [
  {
    name: 'Thea Oldenburger',
    lessons: 4,
  },
  {
    name: 'Vera Bosch',
    lessons: 2,
  },
  {
    name: 'Sharine Dulos',
    lessons: 3,
  },
  {
    name: 'Roya de Wit',
    lessons: 1,
  },
];
