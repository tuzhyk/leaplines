export const knowledge = [
    {
      name: 'Food',
      count: 5,
      percent: 80,
      color: '#df2374',
    },
    {
      name: 'Drinks',
      count: 4,
      percent: 60,
      color: '#851545',
    },
    {
      name: 'Service',
      count: 4,
      percent: 67,
      color: '#999999',
    },
    {
      name: 'Our organization',
      count: 0,
      percent: 100,
      color: '#383838',
    },
  ]; 